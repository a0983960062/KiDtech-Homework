//
//  Constants.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 05/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import Foundation

typealias CompletionHandler = ( _ Success: Bool ) -> ()
