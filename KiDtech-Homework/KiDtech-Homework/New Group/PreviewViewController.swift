//
//  PreviewViewController.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 04/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet var frame: UIImageView!
    
    let imageArray: [UIImage] = [UIImage(named: "frame-circle@4500x4500")!, UIImage(named: "frame-film")!, UIImage(named: "frame-flower@4500x4500")!, UIImage(named: "frame-lace@4500x4500")!, UIImage(named: "frame-wood@4500x4500")!]
    
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        photo.image = self.image
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        let size = CGSize(width: 375, height: 517)
        UIGraphicsBeginImageContext(size)
        
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        photo!.image?.draw(in: areaSize)
        
        frame!.image?.draw(in: areaSize)
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let pngPhoto = UIImagePNGRepresentation(newImage)
        
        let result = UIImage(data:pngPhoto!, scale:1.0)
        
        UIImageWriteToSavedPhotosAlbum(result!, nil, nil, nil)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FrameCollectionViewCell", for: indexPath) as! FrameCollectionViewCell
        
        
        cell.configureCell(image: imageArray[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.frame.image = imageArray[indexPath.row]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
