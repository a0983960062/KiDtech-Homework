//
//  LinkViewController.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 04/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash

class LinkViewController: UIViewController {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.rowHeight = 50
        
        NewsService.instance.getNewsData { (status) in
            if status {
                print(NewsService.instance.newsArray.count)
                self.tableView.reloadData()
            }
        }
        // Do any additional setup after loading the view.
    }

}



extension LinkViewController: UITableViewDataSource, UITableViewDelegate {
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsService.instance.newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LinkTableViewCell") as? LinkTableViewCell else { return UITableViewCell() }
        
        let news = NewsService.instance.newsArray[indexPath.row]
        cell.configCell(title: news.title, description: news.description, link: news.link)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let link = NewsService.instance.newsArray[indexPath.row].link
        guard let webViewController = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as? WebViewController else { return }
        
        webViewController.link = link!
        present(webViewController, animated: true, completion: nil)
    }
}
    







