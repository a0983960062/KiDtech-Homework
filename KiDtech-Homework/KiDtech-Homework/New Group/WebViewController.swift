//
//  WebViewController.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 05/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    @IBAction func backButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    var link: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(link)
        if let url = URL(string: link) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
            
        } else {
            print("can't load url")
        }

        // Do any additional setup after loading the view.
    }

    
}
