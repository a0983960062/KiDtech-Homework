//
//  FrameCollectionViewCell.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 05/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit

class FrameCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var feameImage: UIImageView!
    
    func configureCell(image: UIImage) {
        self.feameImage.image = image
    }
}
