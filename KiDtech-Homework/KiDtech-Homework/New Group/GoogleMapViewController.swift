//
//  GoogleMapViewController.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 02/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class GoogleMapViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var googleMapContainer: UIView!
    
    let locationManager = CLLocationManager()
    
    let linkViewController = LinkViewController()
    
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.showCurrentLoacation()
        self.locationManager.stopUpdatingLocation()
    }
    
    func showCurrentLoacation() {
        
        let camera = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 14)
        
        let latitude = NSString(format: "%.5f", (self.locationManager.location?.coordinate.latitude)!)
        let longitude = NSString(format: "%.5f", (self.locationManager.location?.coordinate.longitude)!)
        
        self.latitudeLabel.text = latitude as String
        self.longitudeLabel.text = longitude as String
        
        let mapView = GMSMapView.map(withFrame: CGRect(x:0, y:0, width:self.googleMapContainer.frame.size.width, height:self.googleMapContainer.frame.size.height), camera: camera)
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "現在位置"
        marker.appearAnimation = .pop
        marker.map = mapView
        self.googleMapContainer.addSubview(mapView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
