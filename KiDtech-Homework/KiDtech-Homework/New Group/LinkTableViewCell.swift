//
//  LinkTableViewCell.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 05/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import UIKit

class LinkTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    
    var link: String = ""
    
    func configCell(title: String, description: String, link: String) {
        self.titleLbl.text = title
        self.descriptionLbl.text = description
        self.link = link
    }

    

}
