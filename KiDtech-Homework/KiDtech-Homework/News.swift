//
//  News.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 05/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import Foundation

struct News {
    
    var title: String!
    var description: String!
    var link: String!
    
}
