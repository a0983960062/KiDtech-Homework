//
//  NewsService.swift
//  KiDtech-Homework
//
//  Created by Charles Chiang on 05/12/2017.
//  Copyright © 2017 Charles Chiang. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

class NewsService {
    
    
    
    var newsArray = [News]()
    
    static let instance = NewsService()
    
    func getNewsData(completion: @escaping CompletionHandler) {
        
        let url = "http://news.ltn.com.tw/rss/focus.xml"
        
        Alamofire.request(url, method: .get, parameters: nil, headers: nil)
            .responseString { (response) in
                let parser = SWXMLHash.parse(response.result.value!)
                let data = parser["rss"]["channel"]["item"]
                
                if response.result.error == nil {
                    for i in data.all {
                        
                        self.newsArray.append(News(title: (i["title"].element?.text)!, description: (i["description"].element?.text)!, link: (i["link"].element?.text)!))
                    }
                    completion(true)
                } else {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
                
                
                
        }
    }
    
    
}
